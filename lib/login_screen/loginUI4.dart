import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:login/login_screen/loginUI5.dart';
import 'package:lottie/lottie.dart';

class LoginUI4 extends StatefulWidget {
  LoginUI4({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _LoginUI4 createState() => _LoginUI4();
}

class _LoginUI4 extends State<LoginUI4> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      backgroundColor: Color(0xff182059),
      body: Center(
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 40,
            ),
            Container(
              height: 150,
              width: 150,
              child: Lottie.asset('assets/lotties/delivery.json'),
            ),
            SizedBox(
              height: 40,
            ),
            Container(
              child: Stack(
                children: <Widget>[
                  Container(
                    height: 345,
                    width: 325,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Container(
                      alignment: Alignment.topLeft,
                      padding: EdgeInsets.symmetric(
                        vertical: 30,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            padding: EdgeInsets.symmetric(
                              horizontal: 20,
                            ),
                            child: Text(
                              'LOGIN',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 30.0),
                            ),
                          ),
                          SizedBox(
                            height: 40,
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(horizontal: 20.0),
                            child: TextField(
                              decoration: InputDecoration(
                                labelText: 'Username',
                                labelStyle: TextStyle(fontSize: 15.0),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(horizontal: 20.0),
                            child: TextField(
                              decoration: InputDecoration(
                                labelText: 'Password',
                                labelStyle: TextStyle(fontSize: 15.0),
                              ),
                              autofocus: false,
                              obscureText: true,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                    top: 30,
                    left: -5,
                    child: Container(
                      height: 40,
                      width: 10,
                      decoration: BoxDecoration(
                          color: Colors.red,
                          borderRadius: BorderRadius.circular(100)),
                    ),
                  ),
                  Positioned(
                      bottom: 115,
                      right: 20,
                      child: GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Container(
                          child: Text(
                            'I Forgot',
                            style: TextStyle(
                                color: Color(0xff37B5F1),
                                fontWeight: FontWeight.w500,
                                fontSize: 15.0),
                          ),
                        ),
                      )),
                  Positioned(
                    bottom: -35,
                    right: 20,
                    child: GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context, 
                          MaterialPageRoute(builder: (context) => LoginUI5()),
                        );
                      },
                      child: Container(
                        height: 75.0,
                        width: 75.0,
                        decoration: BoxDecoration(
                            color: Colors.red,
                            borderRadius: BorderRadius.circular(100)),
                        child: Icon(
                          Icons.arrow_forward,
                          color: Colors.white,
                          size: 35,
                        ),
                      ),
                    ),
                  ),
                ],
                overflow: Overflow.visible,
              ),
            ),
            SizedBox(
              height: 50,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'New User?',
                  style: TextStyle(color: Colors.white),
                ),
                Text(
                  ' Sign Up',
                  style: TextStyle(color: Color(0xff37B5F1)),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
