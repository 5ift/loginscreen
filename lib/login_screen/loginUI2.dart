import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:login/login_screen/loginUI3.dart';

class LoginUI2 extends StatefulWidget {
  LoginUI2({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _LoginUI2 createState() => _LoginUI2();
}

class _LoginUI2 extends State<LoginUI2> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      backgroundColor: Color(0xffF2F2F2),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              height: 50,
            ),
            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.symmetric(horizontal: 40.0),
              child: Text(
                'Login',
                style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
              ),
            ),
            SizedBox(
              height: 25,
            ),
            Container(
              child: Stack(
                children: <Widget>[
                  Container(
                    height: 230,
                    width: 290,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Container(
                      alignment: Alignment.topLeft,
                      padding: EdgeInsets.symmetric(
                        vertical: 30,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            padding: EdgeInsets.symmetric(
                              horizontal: 20,
                            ),
                            child: Text(
                              'Hello!!',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 20.0),
                            ),
                          ),
                          SizedBox(
                            height: 8,
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(horizontal: 20.0),
                            child: TextField(
                              decoration: InputDecoration(
                                labelText: 'Username',
                                labelStyle: TextStyle(fontSize: 12.0),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(horizontal: 20.0),
                            child: TextField(
                              decoration: InputDecoration(
                                labelText: 'Password',
                                labelStyle: TextStyle(fontSize: 12.0),
                              ),
                              autofocus: false,
                              obscureText: true,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                    top: -60,
                    right: 20,
                    child: Container(
                      height: 30,
                      width: 30,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(100),
                        gradient: LinearGradient(
                            begin: Alignment.bottomLeft,
                            end: Alignment.topRight,
                            colors: [Color(0xff37B5F1), Color(0xff843CF7)]),
                      ),
                    ),
                  ),
                  Positioned(
                    top: -125,
                    right: -90,
                    child: Container(
                      height: 100,
                      width: 100,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(100),
                        gradient: LinearGradient(
                            begin: Alignment.bottomLeft,
                            end: Alignment.topRight,
                            colors: [Color(0xff37B5F1), Color(0xff843CF7)]),
                      ),
                    ),
                  ),
                  Positioned(
                    left: -20,
                    child: Container(
                      height: 230,
                      width: 7.5,
                      decoration: BoxDecoration(
                          color: Color(0xff37B5F1),
                          borderRadius: BorderRadius.circular(0)),
                    ),
                  ),
                  Positioned(
                      bottom: 60,
                      right: 20,
                      child: Container(
                        child: Text(
                          'I Forgot',
                          style: TextStyle(
                              color: Color(0xff37B5F1),
                              fontWeight: FontWeight.w500,
                              fontSize: 13),
                        ),
                      )),
                ],
                overflow: Overflow.visible,
              ),
            ),
            GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTap: () {
                Navigator.push(
                  context, 
                  MaterialPageRoute(builder: (context) => LoginUI3()),
                );
              },
              child: Container(
                margin: EdgeInsets.only(left: 300.0, top: 40.0,),
                alignment: Alignment.center,
                height: 50,
                width: 125,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.centerLeft,
                      end: Alignment.centerRight,
                      colors: [Color(0xff37B5F1), Color(0xff843CF7)]),
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(100),
                    bottomLeft: Radius.circular(100),
                  ),
                ),
                child: Text(
                  'Login',
                  style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: Colors.white),
                )
              ),
            ),
            SizedBox(
              height: 30,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  width: 80,
                  height: 1,
                  color: Color(0xff707070),
                ),
                SizedBox(
                  width: 10,
                ),
                Text(
                  'Social Login',
                  style: TextStyle(color: Color(0xff707070)),
                ),
                SizedBox(
                  width: 10,
                ),
                Container(
                  width: 80,
                  height: 1,
                  color: Color(0xff707070),
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Container(
                      height: 30,
                      width: 30,
                      child:
                          Image(image: AssetImage('assets/images/facebook.png'))),
                ),
                SizedBox(
                  width: 15,
                ),
                Container(
                    height: 30,
                    width: 30,
                    child: Image(
                        image: AssetImage('assets/images/instagram.png'))),
                SizedBox(
                  width: 15,
                ),
                Container(
                    height: 30,
                    width: 30,
                    child:
                        Image(image: AssetImage('assets/images/twitter.png'))),
                SizedBox(
                  width: 15,
                ),
                Container(
                    height: 30,
                    width: 30,
                    child:
                        Image(image: AssetImage('assets/images/linkedin.png'))),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'New User?',
                  style: TextStyle(color: Colors.black),
                ),
                Text(
                  ' Sign Up',
                  style: TextStyle(color: Color(0xff37B5F1)),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
