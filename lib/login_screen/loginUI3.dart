import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:math';
import 'package:login/login_screen/loginUI4.dart';

class LoginUI3 extends StatefulWidget {
  @override
  _LoginUI3State createState() => _LoginUI3State();
}

class _LoginUI3State extends State<LoginUI3> {

  bool _showPassword = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Stack(
            children: <Widget>[
              Positioned(
                top: -60,
                left: 250.0,
                child: Transform.rotate(
                  angle: (-pi / 6) + 14,
                  child: Image.asset('assets/images/gradient_hexagon.png', height: 270.0,)
                ),
              ),
              Positioned(
                top: 150,
                right: -20.0,
                child: Transform.rotate(
                  angle: (pi / 2) + 55,
                  child: Image.asset('assets/images/gradient_hexagon_2.png', height: 750.0,)
                ),
              ),
              Container(
                height: MediaQuery.of(context).size.height * 0.7,
                margin: EdgeInsets.only(top: 200),
                padding: EdgeInsets.only(left: 30.0, right: 60.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      'LOGIN',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 30.0,
                        fontWeight: FontWeight.bold,
                        letterSpacing: 1.1,
                      ),
                    ),
                    SizedBox(height: 80.0),
                    Container(
                      child: Text(
                        'Email',
                        style: TextStyle(
                          fontSize: 16.0,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    TextField(
                      decoration: InputDecoration(
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey[300])
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey[300])
                        )
                      ),
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(height: 40.0),
                    Container(
                      child: Text(
                        'Password',
                        style: TextStyle(
                          fontSize: 16.0,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    TextField(
                      obscureText: !this._showPassword,
                      decoration: InputDecoration(
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey[300])
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey[300])
                        ),
                        suffixIcon: IconButton(
                          icon: Icon(
                            Icons.remove_red_eye,
                            color: this._showPassword
                                ? Colors.white
                                : Colors.white30,
                          ),
                          onPressed: () {
                            setState(() => this._showPassword = !this._showPassword);
                          },
                        ),
                      ),
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                    SizedBox(height: 20.0),
                    GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Container(
                        alignment: Alignment.bottomRight,
                        child: Text(
                          'Forgot Password?',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                    Stack(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(top: 30.0, left: 270.0),
                          child: Image.asset('assets/images/white_hexagon.png'),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 270.0, top: 35.0),
                          child: IconButton(
                            onPressed: () {
                              Navigator.push(
                                context, 
                                MaterialPageRoute(builder: (context) => LoginUI4()),
                              );
                            },
                            icon: Icon(Icons.arrow_forward),
                            iconSize: 35.0,
                            color: Color(0xffD9466B),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
