import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';

class LoginUI5 extends StatefulWidget {
  LoginUI5({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _LoginUI5State createState() => _LoginUI5State();
}

class _LoginUI5State extends State<LoginUI5> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Column(
      children: <Widget>[
        Container(
          child: Stack(
            children: <Widget>[
              ClipPath(
                clipper: WaveClipperTwo(flip: true),
                child: Container(
                  height: 242,
                  color: Colors.blue[100],
                ),
              ),
              ClipPath(
                clipper: WaveClipperTwo(),
                child: Container(
                  height: 260,
                  color: Colors.blue[50],
                ),
              ),
              ClipPath(
                clipper: WaveClipperOne(),
                child: Container(
                  height: 240.0,
                  color: Color(0xff1566E0),
                  child: Container(
                    child: Center(
                        child: Image.network(
                      "https://www.freelogodesign.org/file/app/client/thumb/79019595-dad6-44ff-9dfb-70116b03369e_1000x600-watermark.png?20200909",
                      width: 150.0,
                    )),
                  ),
                ),
              ),
            ],
          ),
        ),
        Padding(padding: EdgeInsets.only(top: 20.0)),
        Container(
          width: 300.0,
          child: TextField(
            decoration: InputDecoration(
              prefixIcon: Icon(
                Icons.email,
                color: Colors.blue,
              ),
              border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.blue),
                  borderRadius: BorderRadius.circular(20.0)),
              hintText: "Email",
            ),
          ),
        ),
        Padding(padding: EdgeInsets.only(top: 20.0)),
        Container(
          width: 300.0,
          child: TextField(
            obscureText: true,
            decoration: InputDecoration(
              prefixIcon: Icon(
                Icons.vpn_key,
                color: Colors.blue,
              ),
              border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.blue),
                  borderRadius: BorderRadius.circular(20.0)),
              hintText: "Password",
            ),
          ),
        ),
        Padding(padding: EdgeInsets.only(top: 15.0)),
        ButtonTheme(
          minWidth: 300.0,
          height: 50.0,
          child: FlatButton(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0),
            ),
            color: Colors.blue[700],
            textColor: Colors.white,
            onPressed: () {},
            child: Text("LogIn",
                style: TextStyle(
                  fontSize: 17.0,
                  fontWeight: FontWeight.bold,
                )),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 10.0),
        ),
        GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Container(
            child: Text("FORGOT PASSWORD ?",
                style: TextStyle(
                    fontSize: 15.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.blue[700])),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 25.0),
        ),
        Container(
            child: Text(
          "or",
          style: TextStyle(
              fontSize: 17.0, fontWeight: FontWeight.bold, color: Colors.grey),
        )),
        Container(
            child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
                width: 50.0,
                child: Image.network(
                    "https://i2.wp.com/www.patchamhigh.brighton-hove.sch.uk/wp-content/uploads/2017/05/facebook-logo-circle-new.png?fit=300%2C300")),
            Padding(padding: EdgeInsets.only(left: 15.0)),
            Container(
              width: 45.0,
              child: Image.network(
                  "https://www.pngfind.com/pngs/m/349-3493616_instagram-circle-logo-transparent-hd-png-download.png"),
            ),
            Padding(padding: EdgeInsets.only(left: 15.0)),
            Container(
                width: 40.0,
                child: Image.network(
                    "https://toppng.com/public/uploads/preview/circle-twitter-logo-png-11536001225vm30ml2dw2.png")),
            Padding(padding: EdgeInsets.only(left: 15.0)),
            Container(
                width: 40.0,
                child: Image.network(
                    "http://www.oceanridgebio.com/sites/default/files/inline-images/linkedin-circle-icon_1.png")),
          ],
        )),
        Padding(
          padding: EdgeInsets.only(top: 20.0),
        ),
        Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text("New User? ", style: TextStyle(fontWeight: FontWeight.bold)),
              FlatButton(
                onPressed: () {},
                child: Text(
                  "Sign Up",
                  style: TextStyle(
                    fontWeight: FontWeight.bold, color: Colors.blue
                  )
                )
              )
            ],
          ),
        )
      ],
    ));
  }
}
